<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->integer('category_id')->nullable();
            $table->integer('user_id');
            $table->string('title');
            $table->text('description')->nullable();
            $table->integer('price');
            $table->integer('duration');
            $table->json('languages')->nullable();
            $table->json('benefits')->nullable();
            $table->text('accomplishments')->nullable();
            $table->string('level')->nullable();
            $table->integer('exprience_years')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
