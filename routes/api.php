<?php

use App\Http\Middleware\BasicMiddleware;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function() {

     Route::group(['middleware' => BasicMiddleware::class], function () {

         Route::post('/login', 'UserController@readByAuth');

    });

    Route::group(['prefix' => 'users'], function () {

        Route::post('/', 'UserController@create')->name('create.user');

        Route::group(['middleware' => 'api-auth'], function () {

            Route::get('/', 'UserController@read')->name('get.user');

        });

    });

    Route::group(['prefix' => 'categories'], function () {

        Route::get('/', 'CategoryController@index')->name('categories');

    });

    Route::group(['prefix' => 'slots'], function () {

        Route::get('/{id}', 'SlotController@read')->name('slots');

        Route::group(['middleware' => 'api-auth'], function () {

            Route::get('/', 'SlotController@index')->name('get.slots');

            Route::post('/', 'SlotController@create')->name('create.slots');

            Route::group(['prefix' => '{id}'], function () {

                Route::delete('/', 'SlotController@delete')->name('delete.slots');

            });

        });

    });

    Route::group(['prefix' => 'lessons'], function () {

        Route::group(['middleware' => 'api-auth'], function() {

            Route::post('/', 'GigController@create')->name('lesson.create');

            Route::get('/forAuth', 'GigController@forAuth')->name('lessons.forAuth');

            Route::put('/{id}', 'GigController@update')->name('lesson.update');

            Route::delete('/', 'GigController@deleteMany');

        });

        Route::get('/', 'GigController@index')->name('lessons');

        Route::group(['prefix' => '/{id}'], function () {

            Route::get('/', 'GigController@read')->name('lesson.read');

        });

    });

});

