<?php

namespace App\Http\Middleware;

use App\Exceptions\HttpException;
use App\Models\User;
use Closure;
use Throwable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class BasicMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     * @throws Throwable
     */
    public function handle(Request $request, Closure $next)
    {
        /** @var User $user */
        $user = User::query()
            ->where('email', $request->getUser())
            ->firstOrFail();

        if (Hash::check($request->getPassword(), $user->password)) {
            Auth::setUser($user);
        }

        throw_unless(
            Auth::check(),
            new HttpException(401, 'HTTP_UNAUTHORIZED')
        );

        return $next($request);
    }
}
