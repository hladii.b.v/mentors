<?php

namespace App\Http\Middleware;

use App\Models\User;
use App\Exceptions\HttpException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Closure;
use Throwable;

/**
 * Class RequirePaymentMiddleware
 * @package Core\Http\Middleware
 */
class ApiAuth
{
    public const ROUTE_NAME = 'api-auth';

    /**
     * @param Request $request
     * @param Closure $next
     * @return mixed
     * @throws Throwable
     */
    public function handle(Request $request, Closure $next)
    {
        if (Auth::check() === false && ($token = $request->bearerToken())) {
            /** @var User $authorization */
            $authorization = User::query()->where('api_token', $token)->first();
        }

        if (isset($authorization)) {
            Auth::setUser($authorization);
        }

        throw_unless(
            Auth::check(),
            new HttpException(401, 'HTTP_UNAUTHORIZED')
        );

        return $next($request);
    }
}
