<?php

namespace App\Http\Resources;

use App\Models\Slot;
use Illuminate\Http\Request;

/**
 * Class GigResource
 * @package App\Http\Resources
 * @property Slot $resource
 */
class SlotResource extends JsonResource
{
    /**
     * @var string
     */
    public static $wrap = 'slot';

    /**
     * @var string
     */
    public static $collectionWrap = 'slots';

    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id'          => $this->resource->id,
            'user_id'     => $this->resource->user_id,
            'type'        => $this->resource->type,
            'day_of_week' => $this->resource->day_of_week,
            'start_at'    => $this->resource->start_at,
            'end_at'      => $this->resource->end_at,
        ];

        return $data;
    }
}
