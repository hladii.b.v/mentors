<?php

namespace App\Http\Resources;

use App\Models\Category;
use Illuminate\Http\Request;

/**
 * Class GigResource
 * @package App\Http\Resources
 * @property Category $resource
 */
class CategoryResource extends JsonResource
{
    /**
     * @var string
     */
    public static $wrap = 'category';

    /**
     * @var string
     */
    public static $collectionWrap = 'categories';

    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id'          => $this->resource->id,
            'title'       => $this->resource->title,
            'img'         => $this->resource->img,
            'description' => $this->resource->description,
            'priority'    => $this->resource->priority,
        ];

        return $data;
    }
}
