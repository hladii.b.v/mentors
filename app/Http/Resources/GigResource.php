<?php

namespace App\Http\Resources;

use App\Models\Gig;
use Illuminate\Http\Request;

/**
 * Class GigResource
 * @package App\Http\Resources
 * @property Gig $resource
 */
class GigResource extends JsonResource
{
    /**
     * @var string
     */
    public static $wrap = 'gig';

    /**
     * @var string
     */
    public static $collectionWrap = 'gigs';

    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id'              => $this->resource->id,
            'category_id'     => $this->resource->category_id,
            'title'           => $this->resource->title,
            'user_id'         => $this->resource->user_id,
            'price'           => $this->resource->price,
            'duration'        => $this->resource->duration,
            'languages'       => $this->resource->languages,
            'benefits'        => $this->resource->benefits,
            'description'     => $this->resource->description,
            'accomplishments' => $this->resource->accomplishments,
            'level'           => $this->resource->level,
            'exprience_years' => $this->resource->exprience_years,
            'verified'        => $this->resource->verified,
        ];

        if ($this->resource->relationLoaded('category')) {
            $data['category'] = $this->resource->category ? CategoryResource::make($this->resource->category)->toArray($request) : null;
        }

        if ($this->resource->relationLoaded('user')) {
            $data['user'] = $this->resource->user ? UserResource::make($this->resource->user)->toArray($request) : null;
        }

        return $data;
    }
}
