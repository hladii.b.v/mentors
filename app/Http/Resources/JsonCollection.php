<?php

namespace App\Http\Resources;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * Class JsonCollection
 * @package Core\Http\Resources
 */
class JsonCollection extends AnonymousResourceCollection
{
    /**
     * @var array
     */
    protected $data = [
        //
    ];

    /**
     * @param string $offset
     * @param $value
     * @return $this
     */
    public function setData(string $offset, $value)
    {
        $this->data[$offset] = $value;
        return $this;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function toResponse($request)
    {
        $this->additional([
            'status'  => 200,
            'success' => true,
        ]);

        foreach ($this->collection->getIterator() as $key => $resource) {
            if ($resource instanceof JsonResource) {
                $resource->setInCollection(true);
                $resource->setData(
                    [
                        'index' => $key,
                    ] + $this->data
                );
            }
        }

        return parent::toResponse($request);
    }
}
