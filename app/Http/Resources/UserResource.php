<?php

namespace App\Http\Resources;

use App\Models\User;
use Illuminate\Http\Request;

/**
 * Class GigResource
 * @package App\Http\Resources
 * @property User $resource
 */
class UserResource extends JsonResource
{
    /**
     * @var string
     */
    public static $wrap = 'user';

    /**
     * @var string
     */
    public static $collectionWrap = 'users';

    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id'        => $this->resource->id,
            'role'      => $this->resource->role,
            'name'      => $this->resource->name,
            'last_name' => $this->resource->last_name,
            'email'     => $this->resource->email,
            'image'     => $this->resource->image,
            'phone'     => $this->resource->phone,
            'time_zone' => $this->resource->time_zone,
            'api_token' => $this->resource->api_token
        ];

        if ($this->resource->relationLoaded('gigs')) {
            $data['gigs'] = $this->resource->gigs ? CategoryResource::collection($this->resource->gigs)->toArray($request) : null;
        }

        return $data;
    }
}
