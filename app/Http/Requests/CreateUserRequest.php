<?php

namespace App\Http\Requests;

/**
 * Class CreateUserRequest
 * @package App\Http\Requests
 */
class CreateUserRequest extends FormRequest
{
    public function rules ()
    {
        return [
            'role'      => 'required|string|max:255',
            'email'     => 'required|email|max:255',
            'name'      => 'nullable|string|max:255',
            'last_name' => 'nullable|string|max:255',
            'password'  => 'required|string|max:255',
            'phone'     => 'nullable|string|max:255',
            'time_zone' => 'nullable|string|max:255',
        ];
    }

    public function blacklist()
    {
        return [
            'password'
        ];
    }
}
