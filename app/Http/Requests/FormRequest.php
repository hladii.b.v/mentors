<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest as Request;

/**
 * Class FormRequest
 * @package Core\Http
 */
class FormRequest extends Request
{
    /**
     * @return array
     */
    public function whitelist()
    {
        return array_keys($this->rules());
    }

    public function blacklist()
    {
        return [];
    }

    /**
     * @return array
     */
    public function onlyWhitelist()
    {
        return $this->only(array_diff($this->whitelist(), $this->blacklist()));
    }

    /**
     * @param array|null $data
     * @return array
     */
    public function whitelistWith(?array $data = [])
    {
        return array_replace_recursive(
            $this->onlyWhitelist(),
            $data
        );
    }

}
