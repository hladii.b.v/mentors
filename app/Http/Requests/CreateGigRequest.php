<?php

namespace App\Http\Requests;

/**
 * Class CreateGigRequest
 * @package App\Http\Requests
 */
class CreateGigRequest extends FormRequest
{
    public function rules ()
    {
        return [
            'title'           => 'required|string|max:255',
            'category_id'     => 'required|numeric',
            'description'     => 'string|max:4048|nullable',
            'price'           => 'required|numeric',
            'duration'        => 'required|numeric|max:60',
            'languages'       => 'array|max:255|nullable',
            'benefits'        => 'array|max:255|nullable',
            'accomplishments' => 'string|max:4048|nullable',
            'level'           => 'string|max:255|nullable',
            'exprience_years' => 'numeric|nullable',
        ];
    }
}
