<?php

namespace App\Http\Requests;
use Illuminate\Validation\Rules\In;

/**
 * Class CreateGigRequest
 * @package App\Http\Requests
 */
class CreateSlotRequest extends FormRequest
{
    public function rules ()
    {
        return [
            'type' => [
                'required',
                new In([
                    'available',
                    'blocked',
                ])
            ],
            'from' => 'required||date_format:Y-m-d H:i',
            'to'   => 'required|date_format:Y-m-d H:i',
        ];
    }
}
