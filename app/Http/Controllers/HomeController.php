<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Response;
use App\Models\Gig;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class HomeController extends Controller
{

    /**
     * @param $type
     * @param $id
     * @return BinaryFileResponse
     */
    public function img($type, $id)
    {
        switch ($type) {
            case 'gig':
                /** @var Gig $gig */
                $gig = Gig::query()->findOrFail($id);
                $user = $gig->user;

                $path = storage_path('app/public/images/users/' . $user->id . '/' . $user->image);
                break;
            default:
                $path = '';
        }

        return Response::download($path);
    }
}
