<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSlotRequest;
use App\Http\Resources\JsonCollection;
use App\Http\Resources\JsonResource;
use App\Http\Resources\SlotResource;
use App\Jobs\CreateSlot;
use App\Jobs\FetchSlots;
use App\Models\Gig;
use App\Models\Slot;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Auth;

class SlotController extends Controller
{
    /**
     * @param Request $request
     * @return JsonCollection|JsonResource|AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $query = Auth::user()->slots();

        return SlotResource::collection(
            $this->paginator($request, $query)
        );
    }

    /**
     * @param Request $request
     * @param int $id
     * @return JsonResource
     */
    public function read(Request $request, int $id)
    {
        /** @var Gig $gig */
        $gig = Gig::query()->findOrFail($id);

        $timeSlot = dispatch_now(
            new FetchSlots(
                Carbon::parse($request->get('date')),
                $gig
            )
        );

        return JsonResource::create($timeSlot, 'slots');
    }

    /**
     * @param CreateSlotRequest $request
     * @return JsonResource
     */
    public function create(CreateSlotRequest $request)
    {
        $slots = dispatch_now(
            new CreateSlot(
                Carbon::parse($request->get('from')),
                Carbon::parse( $request->get('to')),
                $request->get('type')
            )
        );

        return  JsonResource::create($slots, 'slots');
    }

    public function delete (int $id)
    {
        /** @var Slot $slot */
        $slot = Slot::query()->findOrFail($id);

        $slot->delete();

        return new SlotResource($slot);
    }
}
