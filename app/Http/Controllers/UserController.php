<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateUserRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * @return UserResource
     */
    public function readByAuth ()
    {
        return new UserResource(Auth::user());
    }

    public function read ()
    {
        $user = Auth::user();

        return new UserResource($user->load('gigs'));
    }

    /**
     * @param CreateUserRequest $request
     * @return UserResource
     */
    public function create (CreateUserRequest $request)
    {
        $user = User::create(
            $request->onlyWhitelist() + [
                'password' =>  Hash::make($request->get('password'))
            ]
        );

        return new UserResource($user);
    }
}
