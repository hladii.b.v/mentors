<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateGigRequest;
use App\Http\Resources\GigResource;
use App\Http\Resources\JsonCollection;
use App\Models\Category;
use App\Models\Gig;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Auth;

class GigController extends Controller
{
    /**
     * @param Request $request
     * @return JsonCollection|Factory|AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $query = Gig::query()->whereNull('deleted_at');

        if ($request->has('category_id')) {
            $query->where('category_id', $request->get('category_id'));
        }

        return GigResource::collection(
            $this->paginator($request, $query->with('category'))
        );
    }

    /**
     * @param int $id
     * @return GigResource
     */
    public function read(int $id)
    {
        /** @var Gig $gig */
        $gig = Gig::query()->findOrFail($id);

        return new GigResource($gig->load('category', 'user'));
    }

    /**
     * @param CreateGigRequest $request
     * @return GigResource
     */
    public function create(CreateGigRequest $request)
    {
        Category::query()->findOrFail($request->get('category_id'));

        $gig = Auth::user()
            ->gigs()
            ->create(
                $request->onlyWhitelist()
            );

        return new GigResource($gig->load('category', 'user'));
    }

    /**
     * @param int $id
     * @param CreateGigRequest $request
     * @return GigResource
     */
    public function update(int $id, CreateGigRequest $request)
    {
        Category::query()->findOrFail($request->get('category_id'));

        $gig = Auth::user()->gigs()->findOrFail($id);

        $gig->update(
            $request->onlyWhitelist()
        );

        return new GigResource($gig->load('category', 'user'));
    }

    /**
     * @param Request $request
     * @return JsonCollection|Factory|AnonymousResourceCollection
     */
    public function forAuth(Request $request)
    {
        $query = Auth::user()->gigs();

        $query->whereNull('deleted_at');

        if ($request->has('category_id')) {
            $query->where('category_id', $request->get('category_id'));
        }

        return GigResource::collection(
            $this->paginator($request, $query->with('category'))
        );
    }

    /**
     * @param Request $request
     * @return JsonCollection|AnonymousResourceCollection
     */
    public function deleteMany(Request $request)
    {
        $gigs = Auth::user()->gigs()->whereIn('id', $request->get('ids'));

        $lessons = clone $gigs;

        foreach ($gigs->cursor() as $gig) {
            $gig->delete();
        }

        return GigResource::collection($lessons->get());
    }
}
