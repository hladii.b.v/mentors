<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @param Request                                                                                $request
     * @param string                                                                                 $orderBy
     * @param string                                                                                 $orderDirection
     * @param \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Relations\Relation $query
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|mixed
     */
    protected function paginator(Request $request, $query, $orderBy = Model::CREATED_AT, $orderDirection = 'desc')
    {
        $query->orderBy(
            $request->query('order_by', $orderBy),
            $request->query('order_direction', $orderDirection)
        );

        if ($request->query('pagination') === '0') {
            return $query->get();
        }

        return $query->paginate(
            min($request->query('limit', 100), 100),
            ['*'],
            'page',
            $request->query('page', 1)
        );
    }
}
