<?php

namespace App\Http\Controllers;

use App\Http\Resources\CategoryResource;
use App\Http\Resources\JsonCollection;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * @param Request $request
     * @return JsonCollection
     */
    public function index(Request $request)
    {
        $query = Category::query()->whereNull('deleted_at')->orderBy('priority');

        return CategoryResource::collection(
            $this->paginator($request, $query)
        );
    }
}
