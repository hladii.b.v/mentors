<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

/**
 * Class User
 * @package App\Models
 * @property Gig $gigs
 * @property Slot $slots
 */
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role',
        'name',
        'last_name',
        'email',
        'password',
        'api_token',
        'paypal_email',
        'phone',
        'time_zone',
        'image'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @return HasMany
     */
    public function gigs()
    {
        return $this->hasMany(Gig::class);
    }

    /**
     * @return HasMany
     */
    public function slots()
    {
        return $this->hasMany(Slot::class);
    }

    public function sent()
    {
        return $this->hasMany(Message::class, 'sender_id');
    }

    public function unreading()
    {
        return $this->hasMany(Message::class, 'sent_to_id')->where('status', 'u')->get();
    }

    public function received()
    {
        return $this->hasMany(Message::class, 'sent_to_id');
    }

    public function sendMessageTo($recipient, $message, $status)
    {
        return $this->sent()->create([
            'body'       => $message,
            'subject' => 'booking',
            'status'    => $status,
            'sent_to_id' => $recipient,

        ]);
    }
}
