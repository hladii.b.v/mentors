<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Slot
 * @package App\Models
 */
class Slot extends Model
{
    const TYPE_AVAILABLE = 'available';
    const TYPE_BLOCKED   = 'blocked';
    const TYPE_WAITING   = 'waiting';
    const TYPE_RESERVED  = 'reserved';

    protected $fillable = [
        'type',
        'day_of_week',
        'start_at',
        'end_at',
    ];

        /**
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
