<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Gig
 * @property $user_id
 * @property $duration
 * @property Category $category
 * @property User $user
 * @package App\Models
 */
class Gig extends Model
{

    protected $fillable = [
        'title',
        'price',
        'duration',
        'accomplishments',
        'category_id',
        'languages',
        'description',
        'level',
        'exprience_years',
        'benefits',
        'verified'
    ];

    protected $casts = [
        'languages' => 'array',
        'benefits'  => 'array',
        'verified'  => 'bool'
    ];

    /**
     * @return BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function booking(){
        return $this->hasMany(Booking::class, 'gig_id');
    }
}
