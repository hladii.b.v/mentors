<?php

namespace App\Exceptions;
use Symfony\Component\HttpKernel\Exception\HttpException as SymfonyHttpException;
use Throwable;

/**
 * Class HttpException
 * @package App\Exceptions
 */
final class HttpException extends SymfonyHttpException
{
    /**
     * @var array|null
     */
    private ?array $extra;

    /**
     * HttpException constructor.
     * @param int            $statusCode
     * @param string|null    $message
     * @param Throwable|null $previous
     * @param array|null     $extra
     */
    public function __construct(int $statusCode, string $message = null, Throwable $previous = null, ?array $extra = null)
    {
        parent::__construct($statusCode, $message, $previous, [], null);
        $this->extra = $extra;
    }

    /**
     * @return array|null
     */
    public function getExtra()
    {
        return $this->extra;
    }
}
