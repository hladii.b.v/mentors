<?php

namespace App\Exceptions\Callbacks;

use Illuminate\Validation\ValidationException;

/**
 * Class ValidationExceptionCallback
 * @package App\Exceptions\Callbacks
 */
class ValidationExceptionCallback
{
    /**
     * @param ValidationException $exception
     * @return array
     */
    public function __invoke(ValidationException $exception)
    {
        return [
            'errors' => $exception->validator->errors()->toArray()
        ];
    }
}
