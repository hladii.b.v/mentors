<?php

namespace App\Exceptions;

use App\Exceptions\Callbacks\ValidationExceptionCallback;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\{
    AccessDeniedHttpException,
    ConflictHttpException,
    MethodNotAllowedHttpException,
    NotFoundHttpException
};


/**
 * Class Handler
 * @package App\Exceptions
 */
class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        AccessDeniedHttpException::class,
        HttpException::class,
        NotFoundHttpException::class,
        ModelNotFoundException::class,
        MethodNotAllowedHttpException::class,
        ValidationException::class,
    ];

    /**
     * @var array
     */
    protected $transforms = [
        HttpResponseException::class => [
            'status' => 'HTTP_NOT_FOUND',
        ],
        MethodNotAllowedHttpException::class => [
            'status' => 'HTTP_METHOD_NOT_ALLOWED',
        ],
        NotFoundHttpException::class => [
            'status' => 'HTTP_NOT_FOUND',
        ],
        AuthorizationException::class => [
            'status' => 'HTTP_FORBIDDEN',
        ],
        ConflictHttpException::class => [
            'status' => 'HTTP_CONFLICT',
        ],
        ValidationException::class => [
            'status'   => 'HTTP_UNPROCESSABLE_ENTITY',
            'callback' => ValidationExceptionCallback::class,
        ]
    ];

    protected $transformExtends = [
        ModelNotFoundException::class => NotFoundHttpException::class,
        AccessDeniedHttpException::class => AuthorizationException::class,
    ];

    /**
     * Render an exception into an HTTP response.
     * @param \Illuminate\Http\Request $request
     * @param \Throwable               $e
     * @return \Illuminate\Http\JsonResponse
     */
    public function render($request, \Throwable $e)
    {
        $httpException = $this->getHttpException($e);

        $responseData = [
            'success' => false,
            'status'  => $httpException->getStatusCode(),
            'message' => $httpException->getMessage()
        ];

        if ($extra = $httpException->getExtra()) {
            $responseData += $extra;
        }

        if (env('APP_DEBUG') && ($previous = $httpException->getPrevious()) !== null) {
            $responseData['debug'] = [
                'message' => $previous->getMessage(),
                'trace'   => explode("\n", $previous->getTraceAsString())
            ];
        }

        return response()->json($responseData);
    }

    /**
     * @param $exception
     * @param null $follow
     * @return HttpException
     */
    protected function getHttpException ($exception, $follow = null) {
        if ($exception instanceof HttpException) {
            return $exception;
        }

        $class = $follow ?? get_class($exception);

        if (isset($this->transforms[$class])) {
            $instruction = $this->transforms[$class];
            $statusCode = constant(sprintf('%s::%s', Response::class, $instruction['status']));
            $extra = null;

            if (isset($instruction['callback']) && class_exists($instruction['callback'])) {
                $extra = (new $instruction['callback'])($exception);
            }

            return new HttpException(
                $statusCode,
                $instruction['status'],
                $exception,
                $extra
            );
        } elseif (isset($this->transformExtends[$class])) {
            return $this->getHttpException($exception, $this->transformExtends[$class]);
        }

        return new HttpException(
            Response::HTTP_INTERNAL_SERVER_ERROR,
            'HTTP_INTERNAL_SERVER_ERROR',
            $exception
        );
    }
}
