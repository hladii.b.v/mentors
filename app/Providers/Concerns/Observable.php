<?php

namespace App\Providers\Concerns;

use Illuminate\Database\Eloquent\Model;

/**
 * Trait Observable
 * @package Core\Providers\Concerns
 */
trait Observable
{
    /**
     * Register observers
     */
    public function registerObservers()
    {
        foreach ($this->observers as $model => $observer) {
            /** @var Model $model */
            $model::observe($observer);
        }
    }
}
