<?php

namespace App\Providers;

use App\Models\User;
use App\Observers\UserObserver;
use App\Providers\Concerns\Observable;
use Illuminate\Support\ServiceProvider;

class ModelsServiceProvider extends ServiceProvider
{
    use Observable;

    /**
     * @var array
     */
    protected array $observers = [
        User::class => UserObserver::class,
    ];

    /**
     * Boot provider
     */
    public function boot()
    {
        $this->registerObservers();
    }
}
