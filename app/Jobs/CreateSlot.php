<?php

namespace App\Jobs;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

/**
 * Class FetchSlots
 * @package App\Jobs
 */
class CreateSlot extends Job
{
    /**
     * @var Carbon
     */
    protected Carbon $startAt;

    /**
     * @var Carbon
     */
    protected Carbon $endAt;

    /**
     * @var string
     */
    protected string $type;

    /**
     * FetchSlots constructor.
     * @param Carbon $startAt
     * @param Carbon $endAt
     * @param string $type
     */
    public function __construct(Carbon $startAt, Carbon $endAt, string $type)
    {
        $this->startAt = $startAt;
        $this->endAt   = $endAt;
        $this->type    = $type;
    }

    public function handle()
    {
        /** @var User $user */
        $user = Auth::user();
        $slots = collect([]);

        while (true) {
            $dayDiff = $this->endAt->diff($this->startAt)->days;

            $startHour = $this->startAt->format('H:i:s');

            $endDate = (clone $this->endAt);

            if ($dayDiff > 0) {
                $this->startAt->startOfDay();
                $endHour = $endDate->endOfDay()->format('H:i:s');
            } else {
                $endHour = $endDate->format('H:i:s');
            }

            $dayOfWeek = $this->startAt->format('Y-m-d');

            $slots->add(
                $user->slots()
                    ->create(
                        [
                            'type'        => $this->type,
                            'day_of_week' => $dayOfWeek,
                            'start_at'    => $startHour,
                            'end_at'      => $endHour,
                        ]
                    )
            );

            $this->startAt->addDay();

            if ($dayDiff === 0 || $dayDiff === false) break;
        }

        return $slots;
    }
}
