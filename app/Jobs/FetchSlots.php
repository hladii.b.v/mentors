<?php

namespace App\Jobs;

use App\Models\Gig;
use App\Models\Slot;
use Carbon\Carbon;

/**
 * Class FetchSlots
 * @package App\Jobs
 */
class FetchSlots extends Job
{
    /**
     * @var Gig
     */
    protected Gig $gig;

    /**
     * @var Carbon
     */
    protected Carbon $date;

    /**
     * @var array
     */
    protected array $daySlot;

    /**
     * FetchSlots constructor.
     * @param Carbon $date
     * @param Gig $gig
     */
    public function __construct(Carbon $date, Gig $gig)
    {
        $this->date = $date;
        $this->gig = $gig;
    }

    /**
     * @return array
     */
    public function handle()
    {
        $user = $this->gig->user;

        $slots = $user
            ->slots()
            ->where('day_of_week', $this->date);

        $openingHours = (clone $slots)
            ->where('type', Slot::TYPE_AVAILABLE)
            ->get(['start_at', 'end_at'])
            ->toArray();

        $occupiedSlots = (clone $slots)
            ->where('type', '!=', Slot::TYPE_AVAILABLE)
            ->get(['start_at', 'end_at'])
            ->toArray();


        foreach ($occupiedSlots as $occupiedSlot) {
            $openingHours = $this->flatAndClean(
                $this->cutOpeningHours($openingHours, $occupiedSlot)
            );
        }

        $array_of_time = [];

        foreach ($openingHours as $item) {
            $startTime = $item[0];
            $endTime = $item[1];

            $add_mins = $this->gig->duration * 60;
            $start_time = strtotime($startTime);
            $end_time = strtotime($endTime);

            while ($start_time < $end_time && $start_time + $add_mins <= $end_time) {
                $array_of_time[] = date("H:i", $start_time) . ' - ' . date("H:i", $start_time + $add_mins);
                $start_time += $add_mins;

            }
        }

        return $array_of_time;
    }

    protected function timeToNum($time)
    {
        preg_match('/(\d\d):(\d\d)/', $time, $matches);
        return 60 * $matches[1] + $matches[2];
    }

    protected function numToTime($num)
    {
        $m = $num % 60;
        $h = intval($num / 60);
        return ($h > 9 ? $h : "0" . $h) . ":" . ($m > 9 ? $m : "0" . $m);

    }

    protected function sub($a, $b)
    {
        if ($a[0] <= $b[0] and $a[1] >= $b[1]) return [[$a[0], $b[0]], [$b[1], $a[1]]];

        if ($b[1] <= $a[0] or $b[0] >= $a[1]) return [[$a[0], $a[1]]];

        if ($b[0] <= $a[0] and $b[1] >= $a[1]) return [[0, 0]];

        if ($b[0] <= $a[0] and $b[1] <= $a[1]) return [[$b[1], $a[1]]];

        if ($b[1] >= $a[1] and $b[0] >= $a[0]) return [[$a[0], $b[0]]];

        return [$a, $b];
    }

    protected function flatAndClean($intervals)
    {
        $result = [];

        foreach ($intervals as $inter) {
            foreach ($inter as $i) {
                if ($i[0] != $i[1]) {
                    $result[] = [$this->numToTime($i[0]), $this->numToTime($i[1])];
                }
            }
        }
        return $result;
    }

    protected function cutOpeningHours($op_h, $occ_slot)
    {
        $subs = [];

        foreach ($op_h as $oh) {
            $ohn = [$this->timeToNum($oh['start_at']), $this->timeToNum($oh['end_at'])];
            $osn = [$this->timeToNum($occ_slot['start_at']), $this->timeToNum($occ_slot['end_at'])];
            $subs[] = $this->sub($ohn, $osn);
        }

        return $subs;
    }
}
