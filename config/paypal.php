<?php
/**
 * PayPal Setting & API Credentials
 * Created by Raza Mehdi <srmk@outlook.com>.
 */

return [
    'mode'    => env('PAYPAL_MODE', 'sandbox'), // Can only be 'sandbox' Or 'live'. If empty or invalid, 'live' will be used.
    'sandbox' => [
        'username'    => env('sb-7ro8c1554954_api1.business.example.com', ''),
        'password'    => env('BHLC3QFNC3XLHJP9', ''),
        'secret'      => env('AR.CHDREkuvrNx2V7l50zrr56SOmA-F6Jx9C5uSYEt5yfVQsXh-X9KPe', ''),
        'certificate' => env(''),
        'app_id'      => 'APP-80W284485P519543T', // Used for testing Adaptive Payments API in sandbox mode
    ],
    'live' => [
        'username'    => env('sb-7ro8c1554954_api1.business.example.com', ''),
        'password'    => env('BHLC3QFNC3XLHJP9', ''),
        'secret'      => env('AR.CHDREkuvrNx2V7l50zrr56SOmA-F6Jx9C5uSYEt5yfVQsXh-X9KPe', ''),
        'certificate' => env( ''),
        'app_id'      => '', // Used for Adaptive Payments API
    ],

    'payment_action' => 'Sale', // Can only be 'Sale', 'Authorization' or 'Order'
    'currency'       => env('PAYPAL_CURRENCY', 'USD'),
    'billing_type'   => 'MerchantInitiatedBilling',
    'notify_url'     => '', // Change this accordingly for your application.
    'locale'         => '', // force gateway language  i.e. it_IT, es_ES, en_US ... (for express checkout only)
    'validate_ssl'   => true, // Validate SSL when creating api client.
];
